#!/bin/bash

set -eu

mkdir -p /app/data/storage /run/nginx /run/registry/ui

if [[ ! -f /app/data/config.yml ]]; then
    cp /app/code/config-example.yml /app/data/config.yml
fi

yq eval -i ".redis.addr=\"${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}\"" /app/data/config.yml 
yq eval -i ".redis.password=\"${CLOUDRON_REDIS_PASSWORD}\"" /app/data/config.yml 

# https://github.com/Joxit/docker-registry-ui/blob/main/bin/entrypoint
echo "=> Fixing UI config"
sed -e "s,\${URL},${CLOUDRON_APP_ORIGIN}/v2ui," \
    -e "s,\${REGISTRY_TITLE},Cloudron Registry," \
    -e "s,\${PULL_URL},${CLOUDRON_APP_DOMAIN}," \
    /app/code/ui/dist/scripts/docker-registry-ui.js.original > /run/registry/ui/docker-registry-ui.js

if [[ -n "${CLOUDRON_PROXY_AUTH:-}" ]]; then
    cp nginx.conf.template /run/nginx/nginx.conf
else
    sed -e 's,root .*,root /app/code/frontend-customauth;,g' nginx.conf.template > /run/nginx/nginx.conf
fi

echo "=> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i docker-registry

