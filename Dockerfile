FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

RUN mkdir -p /app/code
WORKDIR /app/code

ENV BUILDTAGS include_oss include_gcs

# Distribution. Instructions adapted from BUILDING.txt in the distribution's repo
ARG REGISTRY_VERSION="v2.7.1"
ENV GOPATH=/app/code/gows
RUN go get -d github.com/docker/distribution/cmd/registry && \
    cd $GOPATH/src/github.com/docker/distribution && \
    git reset --hard $REGISTRY_VERSION && \
    CGO_ENABLED=0 make binaries && \
    cp ./bin/registry /app/code/registry && \
    rm -rf $GOPATH

# Docker Registry UI (used with proxy auth)
ARG UI_VERSION=1.5.4
RUN mkdir /app/code/ui && cd /app/code/ui && \
    curl -L https://github.com/Joxit/docker-registry-ui/archive/${UI_VERSION}.tar.gz | tar -zxvf - --strip-components=1 && \
    yarn install && \
    yarn build && \
    cp /app/code/ui/dist/scripts/docker-registry-ui-static.js /app/code/ui/dist/scripts/docker-registry-ui.js.original && \
    ln -sf /run/registry/ui/docker-registry-ui.js /app/code/ui/dist/scripts/docker-registry-ui.js

# Simple information page (used when proxy auth disabled)
COPY frontend-customauth/ /app/code/frontend-customauth

# nginx
RUN rm -rf /var/log/nginx && ln -s /run/nginx /var/log/nginx

# Add supervisor configs
COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/registry/supervisord.log /var/log/supervisor/supervisord.log

COPY config-example.yml start.sh nginx.conf.template /app/code/

CMD [ "/app/code/start.sh" ]

