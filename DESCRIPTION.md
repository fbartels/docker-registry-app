This app packages Docker Distribution <upstream>2.7.1 (Registry UI 1.5.4)</upstream>.

## About

The Docker toolset to pack, ship, store, and deliver content.

This repository's main product is the Open Source Docker Registry implementation for storing and distributing Docker and OCI images using the OCI Distribution Specification. The goal of this project is to provide a simple, secure, and scalable base for building a registry solution or running a simple private registry.

